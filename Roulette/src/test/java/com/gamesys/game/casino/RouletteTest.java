package com.gamesys.game.casino;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import org.junit.BeforeClass;
import org.junit.Test;

import com.gamesys.game.casino.monitor.BetSelection;
import com.gamesys.game.casino.player.Player;

public class RouletteTest {

	private static File tempFile;

	/**
	 * Tests weather the Roulette class can process
	 * a simple player file
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void createTestFile() throws Exception {
		tempFile = File.createTempFile("RoulettePlayerTestFile", ".txt");
		tempFile.deleteOnExit();
	}

	/**
	 * Tests weather the Roulette class could 
	 * process a file with multiple players.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void loadMultiPlayerFileTest() throws Exception {
		BufferedWriter out = new BufferedWriter(new FileWriter(tempFile));

		out.write("Tiki_Monkey");
		out.newLine();
		out.write("Barbara");

		out.close();

		Roulette aRoulette = new Roulette(tempFile);

		assertTrue("No players where created", !aRoulette.getAllPlayers().isEmpty());

		assertEquals("The wrong number of players were added", 2, aRoulette.getAllPlayers().size());
	}

	/**
	 * Tests weather the Roulette class could 
	 * process a file with multiple players with 
	 * each with a dissimilar number of parameters.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void loadComplexMultiPlayerFileTest() throws Exception {
		BufferedWriter out = new BufferedWriter(new FileWriter(tempFile));

		out.write("Tiki_Monkey,1.0,2.0");
		out.newLine();
		out.write("Barbara,2.0");
		out.newLine();
		out.write("Junior");

		out.close();

		Roulette aRoulette = new Roulette(tempFile);

		assertTrue("No players where created", !aRoulette.getAllPlayers().isEmpty());

		assertEquals("The wrong number of players were added", 3, aRoulette.getAllPlayers().size());
	}
	/**
	 * Tests if the Roulette class will dismiss incorrectly 
	 * formatted lines in the file.  
	 * 
	 * @throws Exception if the file is non existent
	 */
	@Test
	public void misformattedPlayerFileTest() throws Exception {
		BufferedWriter out = new BufferedWriter(new FileWriter(tempFile));

		out.write("Tiki_Monkey,1.0,2.0");
		out.newLine();
		out.write("Barbara,2.0 gghh");
		out.newLine();
		out.write("Junior");

		out.close();

		Roulette aRoulette = new Roulette(tempFile);

		assertTrue("No players where created", !aRoulette.getAllPlayers().isEmpty());
		assertEquals("The wrong number of players were added", 2, aRoulette.getAllPlayers().size());
	}
	
	/**
	 * Tests if the Roulette class can correctly process 
	 * multiple bets. 
	 * 
	 * @throws Exception 
	 */
	@Test
	public void multiPlayerBetInputTest() throws Exception {
		BufferedWriter out = new BufferedWriter(new FileWriter(tempFile));

		out.write("Tiki_Monkey");
		out.newLine();
		out.write("Barbara");

		out.close();

		Roulette aRoulette = new Roulette(tempFile);
		
		aRoulette.processNewBet("Tiki_Monkey 2 1.0");
		aRoulette.processNewBet("Barbara EVEN 3.0");
		
		Player aPlayer = aRoulette.getPlayer("Tiki_Monkey");
		
		assertEquals("The bet selection was processed incorrectly", BetSelection.BETWEEN_1_AND_36, aPlayer.getBetSelection());
		assertEquals("The amount of money bet was processed incorrectly", 1.0, aPlayer.getCurrentAmountBet(), 0.01);
		
		aPlayer = aRoulette.getPlayer("Barbara");
		
		assertEquals("The bet selection was processed incorrectly", BetSelection.EVEN, aPlayer.getBetSelection());
		assertEquals("The amount of money bet was processed incorrectly", 3.0, aPlayer.getCurrentAmountBet(), 0.01);
		
		aRoulette.getResultOfBets();
	}
	
	/**
	 * Testing if the Roulette class can correctly process 
	 * multiples bets from a single player.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void concurrentPlayerBetInputTest() throws Exception {
		BufferedWriter out = new BufferedWriter(new FileWriter(tempFile));

		out.write("Tiki_Monkey");
		out.newLine();
		out.write("Barbara");

		out.close();

		Roulette aRoulette = new Roulette(tempFile);
		
		aRoulette.processNewBet("Tiki_Monkey 35 1.0");
		
		Player aPlayer = aRoulette.getPlayer("Tiki_Monkey");
		
		assertEquals("The bet selection was processed incorrectly", BetSelection.BETWEEN_1_AND_36, aPlayer.getBetSelection());
		assertEquals("The amount of money bet was processed incorrectly", 1.0, aPlayer.getCurrentAmountBet(), 0.01);
		
		aRoulette.processNewBet("Tiki_Monkey EVEN 646456.0");
		
		assertEquals("The bet selection was processed incorrectly", BetSelection.EVEN, aPlayer.getBetSelection());
		assertEquals("The amount of money bet was processed incorrectly", 646456.0, aPlayer.getCurrentAmountBet(), 0.01);
		
		aRoulette.getResultOfBets();
	}
	/**
	 * Tests if the Roulette class can ignore bets of incorrect format
	 * and from unknown players.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void misformattedPlayerBetInputTest() throws Exception {
		BufferedWriter out = new BufferedWriter(new FileWriter(tempFile));

		out.write("Tiki_Monkey");
		out.newLine();
		out.write("Barbara");

		out.close();

		Roulette aRoulette = new Roulette(tempFile);
		
		aRoulette.processNewBet("Tiki_Monkey -1 1.0");
		
		Player aPlayer = aRoulette.getPlayer("Tiki_Monkey");
		
		assertEquals("The bet selection was processed incorrectly", BetSelection.NO_BETS, aPlayer.getBetSelection());
		assertEquals("The amount of money bet was processed incorrectly", 0.0, aPlayer.getCurrentAmountBet(), 0.01);
		
		aRoulette.processNewBet("Tiki_Monkey2 1 1.0");
		
		assertEquals("The bet selection was processed incorrectly", BetSelection.NO_BETS, aPlayer.getBetSelection());
		assertEquals("The amount of money bet was processed incorrectly", 0.0, aPlayer.getCurrentAmountBet(), 0.01);
		
		aRoulette.processNewBet("Tiki_Monkey EVEN 80.0");
		
		assertEquals("The bet selection was processed incorrectly", BetSelection.EVEN, aPlayer.getBetSelection());
		assertEquals("The amount of money bet was processed incorrectly", 80.0, aPlayer.getCurrentAmountBet(), 0.01);
		
		aRoulette.processNewBet("Tiki_Monkey -1 1.0");
		
		assertEquals("The bet selection was processed incorrectly", BetSelection.EVEN, aPlayer.getBetSelection());
		assertEquals("The amount of money bet was processed incorrectly", 80.0, aPlayer.getCurrentAmountBet(), 0.01);
		
		aRoulette.getResultOfBets();
	}
	/**
	 * Tests if the Roulette class can output the correct 
	 * result after a multiplayer bet is made.
	 * 
	 * @throws Exception
	 */
	@Test
	public void correctBetResultTest() throws Exception {
		BufferedWriter out = new BufferedWriter(new FileWriter(tempFile));

		out.write("Tiki_Monkey");
		out.newLine();
		out.write("Barbara");

		out.close();

		Roulette aRoulette = new Roulette(tempFile);
		
		aRoulette.processNewBet("Tiki_Monkey 2 1.0");
		aRoulette.processNewBet("Barbara ODD 1.0");
		
		aRoulette.setBallLocation(3);
		aRoulette.getResultOfBets();
		
		Player aPlayer = aRoulette.getPlayer("Tiki_Monkey");
		
		assertEquals("The bet outcome was processed incorrectly", "LOSE", aPlayer.getBetOutcome());
		assertEquals("The amount of money won was processed incorrectly", 0.0, aPlayer.getCurrentWinnings(), 0.1);
		
		Player aPlayer2 = aRoulette.getPlayer("Barbara");
		
		assertEquals("The bet outcome was processed incorrectly", "WIN", aPlayer2.getBetOutcome());
		assertEquals("The amount of money won was processed incorrectly", 2.0, aPlayer2.getCurrentWinnings(), 0.1);
	}
	/**
	 * Tests if the Roulette class can output the correct 
	 * result after 2 multiplayer bets are made.
	 * 
	 * @throws Exception
	 */
	@Test
	public void correctConcurrentBetResultTest() throws Exception {
		BufferedWriter out = new BufferedWriter(new FileWriter(tempFile));

		out.write("Tiki_Monkey");
		out.newLine();
		out.write("Barbara");

		out.close();

		Roulette aRoulette = new Roulette(tempFile);
		
		Player aPlayer = aRoulette.getPlayer("Tiki_Monkey");
		Player aPlayer2 = aRoulette.getPlayer("Barbara");
		
		aRoulette.processNewBet("Tiki_Monkey 2 1.0");
		aRoulette.processNewBet("Barbara ODD 1.0");
		
		aRoulette.setBallLocation(3);

		aRoulette.getResultOfBets();
		
		assertEquals("The bet outcome was processed incorrectly", "LOSE", aPlayer.getBetOutcome());
		assertEquals("The amount of money won was processed incorrectly", 0.0, aPlayer.getCurrentWinnings(), 0.1);
		
		assertEquals("The bet outcome was processed incorrectly", "WIN", aPlayer2.getBetOutcome());
		assertEquals("The amount of money won was processed incorrectly", 2.0, aPlayer2.getCurrentWinnings(), 0.1);
		
		aRoulette.resetDefaultValuesForPlayers();
		
		//==== ~ Second Bet ~ ====
		
		aRoulette.processNewBet("Tiki_Monkey 36 10.0");
		aRoulette.processNewBet("Barbara ODD 2.0");
		
		aRoulette.setBallLocation(36);
		aRoulette.getResultOfBets();
		
		assertEquals("The bet outcome was processed incorrectly", "WIN", aPlayer.getBetOutcome());
		assertEquals("The amount of money won was processed incorrectly", 360.0, aPlayer.getCurrentWinnings(), 0.1);
		
		assertEquals("The bet outcome was processed incorrectly", "LOSE", aPlayer2.getBetOutcome());
		assertEquals("The amount of money won was processed incorrectly", 0.0, aPlayer2.getCurrentWinnings(), 0.1);
	}
	/**
	 * Tests if the Roulette class can output the correct 
	 * result when no bet is made.
	 * 
	 * @throws Exception
	 */
	@Test
	public void correctBetResultWhenNoBetTest() throws Exception {
		BufferedWriter out = new BufferedWriter(new FileWriter(tempFile));

		out.write("Tiki_Monkey");
		out.newLine();
		out.write("Barbara");

		out.close();

		Roulette aRoulette = new Roulette(tempFile);
		
		Player aPlayer = aRoulette.getPlayer("Tiki_Monkey");
		Player aPlayer2 = aRoulette.getPlayer("Barbara");
		
		aRoulette.processNewBet("Tiki_Monkey 36 10.0");
		aRoulette.processNewBet("Barbara ODD 2.0");
		
		aRoulette.setBallLocation(3);
		aRoulette.getResultOfBets();
		
		aRoulette.resetDefaultValuesForPlayers();
		
		aRoulette.setBallLocation(3);
		aRoulette.getResultOfBets();
		
		//==== ~ Tiki_Monkey Bet 1 Result ~ ====
		
		assertEquals("The bet selection was processed incorrectly", BetSelection.NO_BETS, aPlayer.getBetSelection());
		assertEquals("The bet outcome was processed incorrectly", "LOSE", aPlayer.getBetOutcome());
		assertEquals("The amount of money won was processed incorrectly", 0.0, aPlayer.getCurrentWinnings(), 0.1);
		
		//==== ~ Barbara Bet 1 Result ~ ====
		
		assertEquals("The bet selection was processed incorrectly", BetSelection.NO_BETS, aPlayer2.getBetSelection());
		assertEquals("The bet outcome was processed incorrectly", "LOSE", aPlayer2.getBetOutcome());
		assertEquals("The amount of money won was processed incorrectly", 0.0, aPlayer2.getCurrentWinnings(), 0.1);
	}
	@Test
	public void totalBetsAndWinsResultTest() throws Exception {
		BufferedWriter out = new BufferedWriter(new FileWriter(tempFile));

		out.write("Tiki_Monkey,1.0,2.0");

		out.close();

		Roulette aRoulette = new Roulette(tempFile);
		
		Player aPlayer = aRoulette.getPlayer("Tiki_Monkey");
		
		//==== ~ Tiki_Monkey Bet 1 ~ ====
		
		aRoulette.processNewBet("Tiki_Monkey 5 11.0");
		
		aRoulette.setBallLocation(5);
		aRoulette.getResultOfBets();
		
		assertEquals("The total number of bets was processed incorrectly", 3.0, aPlayer.getTotalBets(), 0.01);
		assertEquals("The total number of wins was processed incorrectly", 2.0, aPlayer.getTotalWins(), 0.01);
		
		//==== ~ Tiki_Monkey Bet 2 ~ ====
		
		aRoulette.processNewBet("Tiki_Monkey EVEN 100.0");
		
		aRoulette.setBallLocation(5);
		aRoulette.getResultOfBets();
		
		assertEquals("The total number of bets was processed incorrectly", 4.0, aPlayer.getTotalBets(), 0.01);
		assertEquals("The total number of wins was processed incorrectly", 2.0, aPlayer.getTotalWins(), 0.01);
	}
}
