package com.gamesys.game.casino.player;


import com.gamesys.game.casino.monitor.BetSelection;
/**
 * This class exists to store the specific details of
 * each player.
 * 
 * @author Rilwan
 *
 */
public class Player {
	private float totalWins = 0.0f;
	private float totalBets = 0.0f;

	private float betWinnings = 0.0f;
	private int currentBetValue = 0;
	private float currentAmountBet = 0.0f;
	
	private boolean wonLastBet = false;

	private BetSelection myCurrentBet = BetSelection.NO_BETS;

	private String playerName;
	/**
	 * parses the parameter into a player name, totalWins and totalBets 
	 * 
	 * @param playerInitialData
	 */
	public Player(String[] playerInitialData) {
		switch (playerInitialData.length) {
		case 3:
			String betsOfPlayer = playerInitialData[2];
	
			if (betsOfPlayer != null) {
				if (betsOfPlayer.matches("\\d+[.]\\d+")) {
					totalBets = Float.parseFloat(betsOfPlayer);
				}
			}
		case 2:
			String winsOfPlayer = playerInitialData[1];
	
			if (winsOfPlayer != null) {
				if (winsOfPlayer.matches("\\d+[.]\\d+")) {
					totalWins = Float.parseFloat(winsOfPlayer);
				}
			}
		case 1:
			String nameOfPlayer = playerInitialData[0];
			
			if (nameOfPlayer != null) {
				if (nameOfPlayer.matches("\\w+")) {
					playerName = nameOfPlayer;
				}
			}
			
		}
	}
	/**
	 * Checks if a no bets have been made.
	 * 
	 * @return
	 */
	public boolean hasMadeBet() {
		return myCurrentBet != BetSelection.NO_BETS;
	}
	/**
	 * Gets the name of the player 
	 * 
	 * @return
	 */
	public String getName() {
		return playerName;
	}
	/**
	 * Gets the total amount of bets won.
	 * 
	 * @return
	 */
	public float getTotalWins() {
		return totalWins;
	}
	/**
	 * Gets the total amount of bets.
	 * 
	 * @return
	 */
	public float getTotalBets() {
		return totalBets;
	}
	/**
	 * Gets the type of bet made by player.
	 * 
	 * @return
	 */
	public BetSelection getBetSelection() {
		return myCurrentBet;
	}
	/**
	 * Sets the type of bet for the player.
	 * 
	 * @param currentBet
	 */
	public synchronized void setCurrentBet(String currentBet) {
		myCurrentBet = BetSelection.getBetSelection(currentBet);

		if (myCurrentBet == BetSelection.BETWEEN_1_AND_36) {
			this.currentBetValue = Integer.parseInt(currentBet);
		}
	}
	/**
	 * Resets a number of variables to their
	 * default values before each bet
	 */
	public void resetDefaultValues() {
		betWinnings = 0.0f;
		currentBetValue = 0;
		wonLastBet = false;
		myCurrentBet = BetSelection.NO_BETS;
		currentAmountBet = 0.0f;
	}
	/**
	 * Check if the player won money in the last bet.
	 * 
	 * @return
	 */
	public boolean hasWinnings() {
		return wonLastBet;
	}
	/**
	 * Specifics if the player won in
	 * the last bet.
	 * 
	 * @return
	 */
	public String getBetOutcome() {
		return wonLastBet ? "WIN" : "LOSE";
	}
	public synchronized void setBetOutcome(boolean wonLastBet) {
		this.wonLastBet = wonLastBet;
	}
	/**
	 * Gets the won money in the last bet.
	 * 
	 * @return
	 */
	public float getCurrentWinnings() {
		return betWinnings;
	}
	/**
	 * Sets the amount of money the player wants to bet.
	 * 
	 * @param currentAmountBet
	 */
	public synchronized void setCurrentAmountBet(float currentAmountBet) {
		this.currentAmountBet = currentAmountBet;
	}
	/**
	 * Sets the amount of money win by the player.
	 * 
	 * @param betWinnings
	 */
	public synchronized void setCurrentWinnings(float betWinnings) {
		this.betWinnings = betWinnings; wonLastBet = true;
		add1ToTotalWins();
	}
	/**
	 * Gets the integer value bet by the player.
	 * 
	 * @return
	 */
	public int getCurrentBet() {
		return currentBetValue;
	}
	/**
	 * Gets the amount bet by the player.
	 * 
	 * @return
	 */
	public float getCurrentAmountBet() {
		return currentAmountBet;
	}
	/**
	 * Adds one more won when called.
	 */
	public synchronized void add1ToTotalWins() {
		++totalWins;
	}
	/**
	 * Adds one more bet when called.
	 */
	public synchronized void add1ToTotalBets() {
		++totalBets;
	}
	/**
	 * Returns a String representation of the 
	 * last bet's result.
	 * 
	 * @return
	 */
	public String toStringOfBet() {
		return String.format("%-15s%-10s%-10s%-8s", 
				playerName, myCurrentBet == BetSelection.BETWEEN_1_AND_36 ? currentBetValue : myCurrentBet, 
						getBetOutcome(), betWinnings);
	}
	/**
	 * Returns a String representation of the total bets and wins
	 * 
	 * @return
	 */
	public String toStringOfTotalBetsAndWins() {
		return String.format("%-15s%-13s%-9s", 
				playerName, totalWins, totalBets);
	}
	/**
	 * Prints out the string representation of the 
	 * class instance.
	 */
	public String toString() {
		return toStringOfBet();
	}
}
