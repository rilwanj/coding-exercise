package com.gamesys.game.casino;

import java.io.*;
import java.util.*;

import com.gamesys.game.casino.monitor.RandomNumberSelector;
import com.gamesys.game.casino.player.Player;
/**
 * This is a casino game
 * 
 * @author Rilwan
 *
 */
public final class Roulette extends Thread {

	private HashMap<String, Player> allPlayers;
	private RandomNumberSelector aRandomNumberSelector;
	private boolean threadActive = true;
	private int totalSetOfBets = 0;
	
	private File playerFile;

	private Scanner inputRetriever;
	/**
	 * Parses and processes the file data 
	 * into a list the player. 
	 * 
	 * @param aFile
	 * @throws Exception
	 */
	public Roulette(File aFile) throws Exception {
		playerFile = aFile;
		
		allPlayers = new HashMap<String, Player>();
		
		inputRetriever = new Scanner(System.in);

		if (aFile.exists()) {
			loadPlayers(aFile);
			aRandomNumberSelector = new RandomNumberSelector();
		}
	}
	/**
	 * Starts the Game.
	 */
	public void startGame() {
		aRandomNumberSelector.start();
		start();
	}
	/**
	 * Ends the Game.
	 * @throws IOException 
	 */
	public void endGame() throws IOException {
		aRandomNumberSelector.terminate();
		terminate();
		
		BufferedWriter out = new BufferedWriter(new FileWriter("ResultOf" + playerFile));
		for (Player aPlayer : getAllPlayers()) {
			out.write(aPlayer.getName() + "," + aPlayer.getTotalWins() + "," + aPlayer.getTotalBets());
			out.newLine();
		}
		out.close();
	}
	/**
	 * Stops this thread and prints the total bets 
	 * and wins of each player.
	 */
	private void terminate() {
		//safer than thread.stop() function
		threadActive = false;
	}
	private void printPlayerStats() {
		System.out.printf("%-15s%-13s%-9s\n", 
				"Player", "Total Wins", "Total Bets");
		System.out.println("---");

		for (Player aPlayer : getAllPlayers()) {
			System.out.println(aPlayer.toStringOfTotalBetsAndWins()); 
		}
	}
	/**
	 * Sets the player's detail
	 * 
	 * @param playerInitialData
	 */
	private void setPlayer(String[] playerInitialData) {
		allPlayers.put(playerInitialData[0], new Player(playerInitialData));
	}
	/**
	 * Parses and processes the file data 
	 * into a list the player. 
	 * 
	 * @param aFile
	 * @throws Exception
	 */
	private void loadPlayers(File aFile) throws Exception {
		BufferedReader in = new BufferedReader(new FileReader(aFile));

		String nextPlayerData = new String();

		while ((nextPlayerData = in.readLine()) != null) {
			if (nextPlayerData.matches("\\w+(,\\d+[.]\\d+){0,2}")) {
				setPlayer(nextPlayerData.split(","));
			}
		}
	}
	/**
	 * Gets all players.
	 * 
	 * @return
	 */
	public Collection<Player> getAllPlayers() {
		return allPlayers.values();
	}
	/**
	 * gets the player with 'playerName'
	 * 
	 * @param playerName
	 * @return
	 */
	public Player getPlayer(String playerName) {
		return allPlayers.get(playerName);
	}
	/**
	 * Gets the total amount of bets made in the game.
	 * 
	 * @return
	 */
	public int getTotalSetOfBets() {
		return totalSetOfBets;
	}
	/**
	 * Return a randomly generated ball location, so the Roulette class
	 * can be identify the result of each player's bet. 
	 * 
	 * @return
	 */
	public int getBallLocation() {
		return aRandomNumberSelector.getBallLocation();
	}
	/**
	 * Sets a location of which Roulette ball is
	 * placed.
	 * 
	 * @param aLocation
	 */
	public void setBallLocation(int aLocation) {
		aRandomNumberSelector.setBallLocation(aLocation);
	}
	
	public static void main(String[] args) {
		if (args.length == 1) {
			File aFileOfPlayers = new File(args[0]);

			if (aFileOfPlayers.exists() && aFileOfPlayers.isFile()) {
				
				try {
					Roulette aRoulette = new Roulette(aFileOfPlayers);
					
					System.out.println("The Game has started, to terminate input: exit");
					
					aRoulette.startGame();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			System.out.println("No argument was found!");
			System.out.println("Use the following format: ");
			System.out.println("java -jar Roulette <your file>.<extension>");
		}
	}
	/**
	 * Displays the result of each players bet.
	 */
	public synchronized void getResultOfBets() {

		int ballLocation = getBallLocation();
		
		for (Player aPlayer : allPlayers.values()) {
			aPlayer.add1ToTotalBets();

			int betOfPlayer = aPlayer.getCurrentBet();
			
			if (!aPlayer.hasMadeBet()) {
				continue;
			}
			
			switch (aPlayer.getBetSelection()) {
			case EVEN:
				if (ballLocation % 2 == 0) {
					aPlayer.setCurrentWinnings(2 * aPlayer.getCurrentAmountBet());
				}
				break;
			case ODD:
				if (ballLocation % 2 == 1) {
					aPlayer.setCurrentWinnings(2 * aPlayer.getCurrentAmountBet());
				}
				break;
			case BETWEEN_1_AND_36:
				if ((ballLocation == betOfPlayer) && ballLocation >= 1 && ballLocation <= 36) {
					aPlayer.setCurrentWinnings(36 * aPlayer.getCurrentAmountBet());
				}
				break;
			default:
				aPlayer.setBetOutcome(false);
			}
		}
		System.out.printf("Number: %d\n", ++totalSetOfBets);
		System.out.format("%-15s%-10s%-10s%-8s\n",
				"Player", "Bet", "Outcome", "Winnings");
		System.out.println("---");

		for (Player aPlayer : allPlayers.values()) {
			System.out.println(aPlayer); 
		}
		
		System.out.println("\n");
		
		printPlayerStats();
	}
	/**
	 * Resets a number of variables (from each player) to their
	 * default values before each bet
	 */
	public void resetDefaultValuesForPlayers() {
		for (Player aPlayer : allPlayers.values()) {
			aPlayer.resetDefaultValues(); 
		}
	}
	/**
	 * Processes each player's bet.
	 * 
	 * @param betDataOfPlayer
	 */
	public synchronized void processNewBet(String betDataOfPlayer) {
		if (betDataOfPlayer.matches("\\w+ (\\d+|(EVEN)|(ODD)) \\d+[.]\\d+")) {
			
			String[] betData = betDataOfPlayer.split(" ");
			
			if (allPlayers.containsKey(betData[0])) {
			
				Player aPlayer = allPlayers.get(betData[0]);
	
				aPlayer.setCurrentBet(betData[1]);
				aPlayer.setCurrentAmountBet(Float.parseFloat(betData[2]));
			
			}
		}
	}
	@Override
	public void run() {
		end: while (threadActive) {
			
			String betDataOfPlayer = "";

			ArrayList<String> tempPlayerNameList = new ArrayList<String>(allPlayers.keySet());

			for (int i = 0; i < allPlayers.size(); i++) {
				BET: {
				
					betDataOfPlayer = inputRetriever.nextLine();
					
					if (betDataOfPlayer.equals("exit")) {
						try {
							endGame();
						} catch (IOException e) {
							e.printStackTrace();
						} 
						break end;
					}
					
					StringTokenizer tokenizer = new StringTokenizer(betDataOfPlayer);
					
					if (tokenizer.hasMoreTokens()) {
						
						String playerName = tokenizer.nextToken();
					
						if (!tempPlayerNameList.contains(playerName) && allPlayers.containsKey(playerName)) {
							System.out.printf("%s has just made a bet\n", playerName);
							--i; break BET;
						} else if (!allPlayers.containsKey(playerName)) {
							System.out.printf("%s is not a valid player\n", playerName);
							--i; break BET;
						} else {
							tempPlayerNameList.remove(playerName);
						}
					
						processNewBet(betDataOfPlayer);
					}
				}
			}
			
			getResultOfBets();
			resetDefaultValuesForPlayers();
			
			try {
				Thread.sleep(1L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
