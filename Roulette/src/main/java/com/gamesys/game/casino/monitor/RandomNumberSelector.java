package com.gamesys.game.casino.monitor;

import java.util.Random;

public class RandomNumberSelector extends Thread {
	Random numberGenerator = new Random(); 

	private int ballLocation;
	private boolean threadActive = true;

	/**
	 * Stops the thread safely
	 */
	public void terminate() {
		threadActive = false;
	}
	@Override
	public void run() {
		long currentTime = System.currentTimeMillis();

		while(threadActive) {

			long duration = (System.currentTimeMillis() - currentTime);

			if (duration % 30000 == 0) {
				ballLocation = numberGenerator.nextInt(36);
			}

			try {
				Thread.sleep(1L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * Return a randomly generated ball location, so the Roulette class
	 * can be identify the result of each player's bet. 
	 * 
	 * @return
	 */
	public int getBallLocation() {
		return ballLocation;
	}
	/**
	 * Sets a location of which Roulette ball is
	 * placed.
	 * 
	 * @param aLocation
	 */
	public void setBallLocation(int aLocation) {
		ballLocation = aLocation;
	}
}

