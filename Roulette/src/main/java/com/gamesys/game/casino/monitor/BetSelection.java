package com.gamesys.game.casino.monitor;
/**
 * 
 * @author Rilwan
 *
 */
public enum BetSelection {
	EVEN,
	ODD,
	BETWEEN_1_AND_36, 
	NO_BETS;
	
	/**
	 * Identifies and returns a enum representation
	 * of a bet made by each player. 
	 * 
	 * @param aBet
	 * @return
	 */
	public static BetSelection getBetSelection(Object aBet) {
		if (aBet.toString().equals("EVEN")) {
			return EVEN;
		} else if (aBet.toString().equals("ODD")) {
			return ODD;
		} else {
			return BETWEEN_1_AND_36;
		}
	}
}

